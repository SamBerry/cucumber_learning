# Learning Cucumber #

This repository shows a few different ways scenarios can be written to run tests using cucumber.

As the focus is on looking at the different ways cucumber can be used we'll be testing a very simple class which is a basket you can add or remove things from.  Usually we'd use it to test a system end to end but that would detract from our cucumber focus so I've made everything that is not cucumber as simple as possible.

If you are coming to this fresh you should first read up on how cucumber works here: https://cucumber.io/docs/reference#gherkin

There are three files, two have completed examples in. The third has a feature file where one scenario has steps that are undefined and two scenarios which have no steps at all.  These are for you to complete to cement your cucumber knowledge.

### How do I get set up? ###

* Open terminal
* Navigate to the directory where you have checked this repo out
* `gem install cucumber`


### How do I run my tests? ###
`cucumber`
