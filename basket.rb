class Basket
  attr_reader :items

  def initialize
    @items = []
  end

  def add_items(items)
    @items += items
  end

  def remove_item(item)
    @items.delete_at(@items.index(item) || @items.length)
  end
end