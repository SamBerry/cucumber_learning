Feature: Different items in my basket
  A user can add different items to their basket

  Background:
    Given there are no items in my basket

  Scenario Outline: I can add differnt item types to a basket
    When I add <num_items_1> of <item_type_1>
    And I add <num_items_2> of <item_type_2>
    Then I have <num_items_1> of <item_type_1>
    And I have <num_items_2> of <item_type_2>

    Examples:
      | num_items_1 | item_type_1 | num_items_2 | item_type_2  |
      | 3           | milk        | 4           | bread        |
      | 0           | ketchup     | 2           | mayo         |
      | 124         | porridge    | 4           | golden syrup |

  Scenario: A range of items are added to a basket
    When I add items to my bakset:
      | bread     |
      | milk      |
      | biscuits  |
      | bread     |
      | marmelade |
    Then I have 5 items in my basket
    And I have 2 of bread
