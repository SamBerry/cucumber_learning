When(/^I add (\d*) of (.+)$/) do |num, item|
  num.times{ @basket.add_items [item] }
end

When(/^I add items to my bakset:$/) do |table|
  table.raw.each{ |item| @basket.add_items [item[0]] }
end

Then(/^I have (\d*) of (.+)$/) do |num, item|
  occurances = @basket.items.count(item)
  unless occurances == num
    raise "#{occurances} of #{item} in basket not #{num}"
  end
end
