Transform /^(-?\d+)$/ do |number|
  number.to_i
end

Given(/^there are no items in my basket$/) do
  @basket = Basket.new
end

Given(/^there is an item in my basket$/) do
  @basket = Basket.new
  @basket.add_items ['milk']
end

When(/^I add (\d*) item to my basket$/) do |number_of_items|
  number_of_items.times{ @basket.add_items ['milk'] }
end

When(/^I remove an item from my basket$/) do
  unless @basket.remove_item 'milk'
    raise 'Could not remove item from basket.'
  end
end

Then(/^I have (\d*) item(?:s)? in my basket$/) do |number_of_items|
  unless @basket.items.count ==  number_of_items
    raise "There are #{@basket.items.count} items in my basket, not #{number_of_items}."
  end
end
