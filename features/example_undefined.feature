Feature: A user can remove different items from their basket
  A user can remove different items to their basket

  @ex_1
  Scenario: Remove multiple items from basket
    Given I have a basket containing:
      | magazine  |
      | milk      |
      | biscuits  |
      | bread     |
      | marmelade |
    When I remove items:
      | milk      |
      | bread     |
    Then I have 3 items in my basket
    And my basket contains
      | magazine  |
      | biscuits  |
      | marmelade |

  @ex_2
  Scenario: Cant remove item that doesn't exist

  @ex_3
  Scenario: Cant remove item from empty basket

