Feature: Items in my basket
  As a user I want to know how many items are in my basket

  Scenario: An item is added to an empty basket
    Given there are no items in my basket
    When I add 2 item to my basket
    Then I have 2 items in my basket

  Scenario: An item is removed from a basket
    Given there is an item in my basket
    When I remove an item from my basket
    Then I have 0 items in my basket
